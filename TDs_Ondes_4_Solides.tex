% !TEX root = TDs_Ondes.tex

\clearpage

\section{Ondes dans les milieux solides}

\setcounter{subsection}{-1}
\subsection{Réflexion d'ondes sismiques}

{\bf Première partie :  ondes dans un milieu infini}

On considère tout d'abord, dans le cas général, un milieu solide soumis à un champ de déplacement lagrangien 
$ \vec X(\vec x) = \vec u(\vec x)$  écrit sous la forme suivante :

$$
 \vec u = \vec u_\ell +  \vec u_t
 $$
 avec 


$$\vec u_\ell = A_\ell \frac{\vec k_\ell }{|k_\ell|} e^{ i \vec k_\ell \cdot \vec x- i \omega t}  
= A_\ell \left[   \begin{array}{c} 
\frac{k_{\ell,x}}{ \vec{| k_\ell}| } \\ 0 \\ \frac{k_{\ell,z}}{ \vec{| k_\ell}| } 
\end{array} \right]
e^{ i [ k_{\ell,x} x + k_{\ell,z} z - \omega t]}
=  A_\ell \left[   \begin{array}{c} 
 \cos \theta_\ell  \\ 0 \\ \sin \theta_\ell  
\end{array} \right]
e^{ i[ {\color{red} k_{\ell} ( x\cos \theta_\ell  + z \sin \theta_\ell )- \omega t}]}
$$

$$\vec u_t = A_t \frac{\vec{e_y} \wedge \vec k_t }{|k_t|} e^{ i \vec k_t \cdot \vec x- i \omega t}  
= A_t \left[   \begin{array}{c} 
\frac{+k_{t,z}}{ \vec{| k_t}| } \\ 0 \\ \frac{-k_{t,x}}{ \vec{| k_t}| } 
\end{array} \right]
e^{ i [ k_{t,x} x + k_{t,z} z - \omega t]}
=  A_t \left[   \begin{array}{c} 
 \sin \theta_t \\ 0 \\ - \cos \theta_t  
\end{array} \right]
e^{ i[ {\color{red} k_{t} ( x\cos \theta_t  + z \sin \theta_t )- \omega t}]}
$$

\begin{enumerate}
\item Vérifiez que ces définitions définissent bien des ondes {\em longitudinales} et {\em transverses}, respectivement. 
A quoi correspondent les angles $\theta_{ell}$ et $\theta_t$ ainsi définis ?


\item A partir des lois constitutives du milieu solide (Equation de Cauchy et loi de Hooke) {\color{red} (cf. annexe 6.4)} montrez que ces deux solutions d'ondes vérifient respectivement les relations de dispersions suivantes :
$$
\omega^2 = k_{ell}^2 {c_\ell} ^2, \qquad \mbox{ avec } c_\ell^2 = \frac{E \left(1-\nu \right)}{\rho \left( 1+\nu \right) \left(1 - 2 \nu \right)}
$$

$$
\omega^2 = k_{t}^2 {c_\ell} ^2, \qquad \mbox{ avec } c_t^2 = \frac{E}{2 \rho \left( 1+\nu\right)}
$$

\item Application numérique : calculez la vitesse des ondes longitudinales et transverses pour un manteau terrestre en granite ($E = 60 GPa ; \nu = 0.2$).


{\bf Seconde partie :  réflexion d'ondes }

On considère maintenant un milieu semi-infini (défini par le demi-plan $z<0$). On suppose qu'on observe en surface la propagation d'une élévation de la surface terrestre de la forme 
$$\eta(x,t) = u_z(x,z=0,t)  = C e^{ i k (x - ct )}
$$
où $c$ est la {\em vitesse de phase de l'onde observée en surface.}



On cherche à savoir que type(s) d'onde(s), en profondeur, sont responsables de cette déformation observée.

\item On suppose $c > c_{ell} > c_t$. Montrez que la solution observée est compatible avec 4 solutions d'ondes différentes.
 
Donnez les valeurs de $\theta$ correspondantes. Quelle est  la signification physique des solutions vérifiant respectivement $\theta>0$ et $\theta<0$ ?


\item Ecrire les conditions limites à vérifier par le champ de déformation (et/ou de contraintes). Montrez que dans le cas général
celles-ci permettent de calculer les amplitudes 
$A_{\ell,-} , A_{\ell,-} $ des ondes réfléchies en fonction de celles $A_{\ell,+} , A_{\ell,+} $ des ondes incidentes.

\item On suppose maintenant $c_{ell} > c > c_t$. Montrez qu'il existe 3 solutions d'ondes compatibles avec la déformation observée en surface.
Quelle est la nature de l'onde de type "longitudinale" ?


\item Montrez qu'à l'aide des conditions limites en surface il est possible de calculer les amplitudes de l'onde longitudinale réfléchie 
$A_{\ell,-}$ et celle de l'onde évanescente  $\tilde{A_{t}}$ en fonction de celle de l'onde longitudinale incidente ${A_{\ell,+}}$.

 
{\bf Troisième partie : onde de Rayleigh }

 
 \item On suppose maintenant $c_{ell} > c_t > c$. Combien de solutions d'ondes sont-elles possibles dans ce cas ?
 
 \item Montrez qu'il ne peut exister de solution de ce type que pour une valeur précise de $c$.
 
 {\em Ce cas correspond à l'onde de Rayleigh vue en cours}.
  

\end{enumerate}

\clearpage


\subsection{Ondes de Rayleigh à la surface d'un solide élastique semi-infini}

\noindent Pour pouvoir obtenir une combinaison d'ondes longitudinales et d'ondes transverses satisfaisant les conditions limites pour aboutir à un mode propagatif, nul besoin d'un milieu borné. En effet, un milieu semi-infini suffit, des modes propagatifs concentrés vers la surface pouvant alors parfois être identifiés. Pour les milieux élastiques isotropes homogènes, ces ondes de surface portent le nom d'onde de Rayleigh en hommage à leur découvreur. 

\noindent On s'intéresse à un milieu solide élastique semi-infini de masse volumique $\rho$ dont le module d'Young est noté $E$ et le coefficient de Poisson $\sigma$. On recherche des solutions de l'équation des ondes pour ce milieu capables de se propager au voisinage de la surface de ce solide. Il faut pour cela considérer un mouvement combinant les deux familles d'ondes élastiques trouvées en milieu infini, à savoir les ondes longitudinales d'une part (ou de compression), et les ondes transverses d'autre part.

\begin{enumerate}
\item Comment sont définies mathématiquement les ondes longitudinales d'une part, et transverses d'autre part ?
\item Au moyen de ces propriétés, montrer que l'équation pour les corps élastiques (cf annexe) se transforme en deux équations d'ondes avec comme célérités :
\begin{eqnarray}
c_l^2 = \frac{E \left(1-\sigma \right)}{\rho \left( 1+\sigma \right) \left(1 - 2 \sigma \right)} & et &
c_t^2 = \frac{E}{2 \rho \left( 1+\sigma\right)}
\end{eqnarray} 
\end{enumerate}

\noindent On considère que la surface du solide semi-infini correspond à un axe $(Ox)$ et on nomme $z$ la direction perpendiculaire. La surface au repos est donc définie comme $z=0$. On décompose le champ de déplacement dans le plan vertical $\vec{u}(x,z)$ pour $z<0$ sous la forme :
$$\vec{u}=\left( u_{lx}+u_{tx} \right) \vec{e_x} + \left( u_{lz}+u_{tz} \right) \vec{e_z}$$
où $u_{lx}$ est la composante suivant $x$ du champ de déplacement longitudinal, $u_{tx}$ est la composante suivant $x$ du champ de déplacement transversal, et ainsi de suite.

On considère des solutions propagatives selon $x$ mais localisées au voisinage de $z=0$ pour les ondes longitudinales de ce solide semi-infini. On recherche donc des solutions de la forme :
$$u=\tilde{u}(z) \exp^{i\left(kx - \omega t \right)}$$ (où $u$ est n'importe laquelle des contributions à $\vec{u}$).

\begin{enumerate}
\setcounter{enumi}{2}
\item Montrer que pour les ondes longitudinales, pour $k^2-\omega^2/c_l^2>0$, on obtient comme solutions :
\begin{eqnarray}
u_{lx} & = & A \exp^{\alpha z} \exp^{i\left(kx - \omega t \right)} \\
u_{lz} & = & -i \frac{\alpha}{k} A \exp^{\alpha z} \exp^{i\left(kx - \omega t \right)} \\
\end{eqnarray}
où $A$ est une amplitude libre et $\alpha$ vérifie :
$$\alpha^2=k^2-\frac{\omega^2}{c_l^2}$$
\item Montrer que pour les ondes transverses, pour $k^2-\omega^2/c_t^2>0$, on obtient comme solutions :
\begin{eqnarray}
u_{tx} & = & B \exp^{\beta z} \exp^{i\left(kx - \omega t \right)} \\
u_{tz} & = & -i \frac{k}{\beta} B \exp^{\beta z} \exp^{i\left(kx - \omega t \right)} \\
\end{eqnarray}
où $B$ est une amplitude libre et $\beta$ vérifie :
$$\beta^2=k^2-\frac{\omega^2}{c_t^2}$$
\item Que doivent vérifier les composantes normales et tangentielles du tenseur des contraintes $\Pi_{zz}$ et $\Pi_{xz}$ à la surface du solide en $z=0$ si ce solide donne sur du vide ?
\item Montrer que :
$$c_l^2-2c_t^2=\frac{ \sigma E}{\rho (1+\sigma)(1-2\sigma)}$$
En déduire que les conditions aux limites précédentes peuvent aussi s'écrire :
\begin{eqnarray}
0 & = & c_l^2 \frac{\partial u_z}{\partial z}+\left( c_l^2 - 2 c_t^2 \right) \frac{\partial u_x}{\partial x} \\
0 & = & \frac{\partial u_x}{\partial z} + \frac{\partial u_z}{\partial x}
\end{eqnarray}
\item Montrer que ces conditions imposent alors que :
\begin{eqnarray}
A \left( 2 \alpha \right) + B \left( \beta + k^2/\beta \right) & = & 0 \\ 
A \left( 2k + (\alpha^2/k - k )(c_l^2/c_t^2) \right) + B \left( 2 k \right) & = & 0 \label{eq2}
\end{eqnarray} 
\item Montrer que $(\alpha^2 - k^2 )(c_l^2/c_t^2)=\beta^2 - k^2$ et remplacer dans l'équation (\ref{eq2}). 
\item Montrer alors que ce système de deux équations à deux inconnues n'admet de solution non nulle que si :
$$4 \alpha \beta k^2 = \left( k^2 + \beta^2 \right)^2$$
\item Elever l'équation précédente au carré, remplacer les $\alpha^2$ et $\beta^2$ par leurs expression en $k$ et $\omega$, puis introduire $c=\omega/k$ pour montrer que $c$, la célérité de l'onde, est solution de :
$$\left( 1 - c^2/c_l^2 \right)\left( 1 - c^2/c_t^2 \right)=\left( 1 - c^2/(2c_t^2) \right)^4$$

\noindent
\begin{figure}[!hbt]
\begin{center}
\includegraphics[keepaspectratio=true,width=14cm]{Figures/figurerayleigh}
\end{center}
\caption{Tracé de la fonction en $X$ correspondant au terme de droite de l'équation ($\ref{eqX}$).}
\label{figurerayleigh}
\end{figure}

\item En recherchant une solution pour $c$ sous la forme $c=\sqrt{X} c_t$, où $\sqrt{X}$ est donc un simple coefficient multiplicateur, montrer que la solution du problème précédent revient à chercher la solution de l'équation :
\begin{equation}
\frac{c_t^2}{c_l^2} X = 1 - \frac{\left( 1- \frac{1}{2} X \right)^4}{1-X}
\label{eqX}
\end{equation}
\end{enumerate}

\noindent On a tracé dans la figure \ref{figurerayleigh} la fonction de $X$ qui apparaît dans le terme de droite de l'équation (\ref{eqX}). 

\begin{enumerate}
\setcounter{enumi}{11}
\item Sachant que sauf pour quelques cas exotiques (milieux poreux granulaires par exemple), $0<\sigma<1/2$, quelle est la gamme de valeurs possibles pour $c_t/c_l$ ?
\item En déduire graphiquement les valeurs possibles du coefficient $\sqrt{X}$.
\item Pour de l'acier ($\rho=7850\ kgm^{-3}$, $E=220\ GPa$ et $\sigma=0.29$) et du verre ($\rho=2600\ kgm^{-3}$, $E=60\ GPa$ et $\sigma=0.25$), calculer $c_l$, $c_t$ et la célérité $c$ de l'onde de Rayleigh.
\end{enumerate}




\subsection{Vibrations flexionnelles d'une barre ; application aux instruments de type xylophone}

On souhaite étudier les modes de vibration d'une lame d'un instrument de musique de type xylophone (ou marimba).  Les lames sont des objets de largeur $\ell$ et longueur $L$ constantes, mais en général elles ont une épaisseur $h(x)$ variable dans la direction de la longueur ($0<x<L$).
Les lames sont constituées d'un matériau de masse volumique $\rho$ et module d'Young $E$.
On supposera l'épaisseur maximale $e = max(h(x))$ faible devant la longueur de manière à pouvoir utiliser l'équation d'Euler-Bernouilli.

\begin{enumerate}

\item Montrez que les fréquences propres $\omega$ et les modes propres $\hat y(x)$  de vibration sont solutions de l'équation suivante :

$$ 
\rho S(x)  \omega^2 \hat y(x) = 
\frac{\partial^2  }{\partial x^2 }\left(  E S(x) K(x)^2 \frac{\partial^2 \hat y(x)  }{\partial x^2} \right)
$$

\item 
Justifiez que dans le cas où les barres sont libres aux deux extrémités, les modes propres vérifient en outre les conditions limites suivantes :

$$
{\left( \frac{\partial^3 \hat y}{\partial x^3} \right)}_{x=0} 
= {\left( \frac{\partial^2 \hat y}{\partial x^2} \right)}_{x=0} 
= {\left( \frac{\partial^3 \hat y}{\partial x^3} \right)}_{x=L} 
= {\left( \frac{\partial^2 \hat y}{\partial x^2} \right)}_{x=L} 
= 0.
$$

\item Exprimez $S(x)$ et $K(x)$ en fonction de $h(x)$.

\item On souhaite résoudre le problème numériquement à l'aide d'une méthode de différences finies. Pour celà on introduit un mailllage régulier de l'intervalle $0<x<L$ en posant 
$x_i = (i-1)L/(N-1)$ et on discrétise les fonctions inconnues $\hat y(x)$ en posant 
$y_i = \hat{y}(x_i)$.

En approximant les dérivées d'ordre 2 par des formules centrées, montrez que l'équation différentielle conduit à une relation entre $y_{i-2}, y_{i-1}, y_{i}, y_{i+1}$ et $y_{i+2}$.

\item Donnez également une discrétisation d'ordre 2 des conditions limites en $x=0$ et $x=L$.  

\item Montrez que le problème se ramène alors à un problème aux valeurs propres de la forme suivante :
$$ A Y = \omega^2 B Y$$
 où $A$ et $B$ sont des matrices carrées et $Y = [y_1, y_2, .... y_N]$.

\item Ecrire un programme matlab qui résoud le problème.

\item On considère tout d'abord le cas de barres d'épaisseur constante $h(x)=e$. A l'aide de votre programme, calculez les fréquences des trois premiers modes propres et vérifiez que celles-ci sont données par les relations suivantes :

$$
f_1 =  1.028 \,  e/L^2 \sqrt{E/\rho}  ; \quad  f_2 =  2.756 f_1 ; \quad f_3 =  5.408 f_1.
$$  


%Les noeuds du premier mode sont à $x = 0.22 L$ et $0.78 L$. 

(cf. {\em The  physics of musical instruments}, Fletcher \& Rossing, pp. 63.)

%}

\item Application : donnez la longueur théorique d'une lame de marimba dont la fréquence fondamentale correspond à la note $do_4$. 

La lame est en bois de palissandre de caractéristiques $\sqrt{E/\rho} =  5217 m/s$ et d'épaisseur $e=1cm$.

%% 262 Hz, longueur 45cm.

%\item et représentez la forme des trois premiers modes propres. A quelle position se trouve les noeuds de vibration du premier mode propre ?


\item Le choix de barres d'épaisseur constante présente un défaut : les fréquences propres des modes ne sont pas en rapport harmoniques les unes par rapport aux autres. 
Pour améliorer les propriétés musicales de ces instruments, les facteurs d'instruments utilisent des lames avec une forme creusée dans la partie centrale. 

Une telle forme peut être modélisée par la formule suivante, où $a$ et $b$ sont des paramètres sans dimensions vérifiant $0<a<1/2$ et $0<b<1$. 




$$
h(x) = \left\{  \begin{array}{ll} 
e & \qquad \mbox{ pour } 0<x/L<a \\
e \left[ 1 -b \left( 1 - \frac{(x/L-1/2)^2}{(a-1/2)^2} \right)\right] & \qquad \mbox{ pour }  a<x/L<(1-a)  \\
e & \qquad \mbox{ pour }  (1-a)<x/L<1
\end{array}
\right.
$$

Introduisez cette forme de barre dans votre programme Matlab.


\item On choisira tout d'abord $a=1/4$. A l'aide de votre programme, déterminez la valeur de $b$ permettant de réharmoniser les deux premiers modes de vibration avec le rapport $f_2 /f_1 = 4$. 

\item Que vaut alors $f_1$  dans le cas où les dimensions $L$ et $e$ sont celles de la question 9 ?  Quelle longueur faut-il donner à la barre si l'on souhaite toujours obenir la note $do_4$ ?



%$f_3/f_1$ ?




\item Cherchez maintenant à déterminez les valeurs de $a$ et $b$ permettant de réharmoniser les trois premiers modes de vibration avec les rapports $f_2/f_1 = 4$ et $f_3/f_1 = 9$ (choix ayant des propriétés musicales intéressantes, et recherché par de nombreux facteurs d'instruments). Quelle est maintenant la longueur de la lame correspondant au $do_4$ ?

\end{enumerate}




\subsection{Inharmonicité d'une corde due à la raideur en flexion}

On reprend l'étude des instruments à cordes abordée dans l'exercice 1.1.

Si la section d'une corde n'est pas négligeable devant sa longueur, la rigidité en flexion intervient dans le problème, et l'équation des ondes doit être modifiée ainsi :



$$
\mu \frac{\partial^2 Y}{\partial t^2} = 
T \frac{\partial^2 Y}{\partial x^2} - \frac{\pi E a^4}{4} \frac{\partial^4 Y}{\partial x^4}
$$

%% Rossing : E S K^2
%ou $s$ est la section de la corde et $K$ est le 'rayon de gyration' ($K= a/2$) 

où $a$ est le rayon de la corde. 
%(On verra la justification de cette équation dans un chapitre ultérieur).

On supposera que cette dernière est tenue par un pivot à chaque extrémité, c'est à dire vérifie les conditions limites $Y(x=0) = \frac{\partial^2 Y}{\partial x^2}(x=0) = Y(x=L) = \frac{\partial^2 Y}{\partial x^2}(x=L) = 0$.




\begin{enumerate}

\item 
Montrez que les fréquences propres $\omega_n$ d'une corde de longueur $L$ sont données par la formule suivante :

$$
f_n = n f_1^o (1+B n^2)^{1/2}
$$

où $f_1^o= c/(2 L)$ est la fréquence propre d'une corde de section nulle, et $B$ est le facteur d'inharmonicité, défini par $B = \pi^3 E a^4/(4 T L^2)$.




\item Donnez la valeur de $B$ pour les cordes suivantes :

$(a)$ Corde $mi_4$ d'une guitare "folk" (en acier, de masse volumique $\rho = 7800 kg/m^3$, module d'Young $E = 208 GPa$, autres caractéristiques données dans l'exercice 1).

$(b)$ Corde $mi_4$ d'une guitare classique (en nylon, masse volumique $\rho = 1240 kg/m^3$, module d'Young $E = 2.9 GPa$).

$(c)$ Corde $la_5$ d'un piano (en acier, longueur 208mm, tension $672 N$).



%\begin{array}{| l | l | l |}
%Note & Fréquence &  Longueur & Tension & 
%$do_1$ (corde grave) 
%$do_4$ (corde médium)
%$do_7$ (corde 


\item Reprendre l'exercice 1.1.2 et montrez comment on peut calculer par la méthode de Fourier le mouvement d'une corde pincée de section non négligeable.

\item La méthode de d'Alembert est-elle applicable ? A quel effet physique peut-on s'attendre ?

\end{enumerate}





\subsection{Propagation d'ondes axisymétriques le long d'un tube élastique plein de section circulaire}

\noindent On souhaite ici déterminer les modes propagatifs pour un tube de section circulaire de rayon $R$ constitué d'un matériau élastique homogène isotrope caractérisé par sa masse volumique $\rho$, son module d'Young $E$ et son coefficient de Poisson $\sigma$. Dans un milieu élastique infini, deux familles d'ondes coexistent indépendemment : les ondes longitudinales (ou ondes de compression) avec une célérité $c_\ell$, et les ondes transverses (ou ondes de cisaillement) avec une célérité $c_t$. Dans le cas où le milieu est borné, comme ici, les conditions limites imposent l'existence simultanée de ces deux types de mouvements ondulatoires, la combinaison ainsi réalisée générant des modes propagatifs avec une célérité qui n'est ni $c_\ell$, ni $c_t$ ! Le problème traité ici consiste à déterminer la forme exacte de ces modes propagatifs ainsi que leur relation de dispersion (loi $\omega(k)$ pour des modes monochromatiques).

\begin{enumerate}

\item Rappeler l'équation régissant la dynamique du champ de déplacement $\vec{u}$ dans le cylindre. Quelles sont les conditions limites sur la surface du cylindre ?

\item Décomposer ce champ de déplacement $\vec{u}$ en une partie irrotationelle $u_\ell$ et une partie à divergence nulle $u_t$. Introduire le potentiel scalaire $\varphi$ et le potentiel vecteur $\vec{\Phi}$ servant à décrire respectivement $u_\ell$ et $u_t$.

\item Rappeler les équations d'ondes vérifiées par $\varphi$ et $\vec{\psi}$ en utilisant les notations $c_\ell$ et $c_t$.

\item Pour des modes axisymétriques monochromatiques, montrer qu'on peut chercher les potentiels sous la forme $\varphi=\varphi(r,z) \exp \left( -i\omega t \right)$ et $\vec{\psi}=\psi_\theta(r,z) \exp \left( -i\omega t \right)$. 

\item Ecrire les équations scalaires pour $\varphi$ et $\psi_\theta$. Montrer par séparation de variable que ces équations admettent pour solutions :
\begin{eqnarray}
\varphi(r,z)=A J_0(pr) \exp \left( i(kx-\omega t)\right) & avec & p^2=\omega^2/c_\ell^2-k^2 \\
\psi_\theta(r,z)=C J_1(qr) \exp \left( i(kx-\omega t)\right) & avec & q^2=\omega^2/c_tl^2-k^2 \\
\end{eqnarray}
\noindent où $J_0$ et $J_1$ sont des fonctions de Bessel.

\item En appliquant les conditions limites en $r=R$, montrer que le nombre d'onde longitudinal doit prendre des valeurs discrètes données par l'équation:
$$ \frac{2p}{R} \left( q^2+k^2 \right) J_1 (pR) J_1 (qR) - \left( q^2- k^2 \right) J_0 (pR) J_1 (qR) - 4 k^2 pq J_1 (pR) J_0(qR)=0$$
\noindent où $p$ et $q$ sont des fonctions de $k$ et $\omega$ données plus hautes.

\item Dans la limite $kR \rightarrow 0$, simplifier l'expression précédente pour les modes le plus \og bas \fg (première racine de l'équation aux valeurs propres) et montrer qu'on retrouve le résultat \og classique \fg d'ondes de compression se propageant à la célérité $\sqrt{E/\rho}$ le long d'une barre de faible épaisseur (résultat direct dans le cadre de la théorie des poutres).

\end{enumerate}



%
%\subsection{Propagation du son dans un milieu granulaire (BGB)}
%
%
%\begin{figure}[h]
%\begin{center}
%\input{Figures/Billes1.pdftex_t}
%\caption{Propagation du son le long d'une rangée de billes. En haut,
%géométrie globale. En bas, détail au niveau d'une bille (pointillés :
%configuration à l'équilibre ;ligne continue : déplacement par rapport à
%l'équilibre).}
%\end{center}
%\end{figure}
%
%Dans un milieu granulaire compact, les ondes "sonores" 
%(plus exactement les ondes de compression longitudinales) ont des
%propriétés bien particulières.
%
%Dans ce problème, on étudie le cas simplifié d'une rangée de $N$ billes
%de rayon $a$ et de masse $m$, 
%soumises à une compression axiale  $F$ (cf. figure).
%
%On note $X_n(t)$ la position du centre de gravité de la bille $n$ 
%(mesurée algébriquement
%dans la direction $x$ indiquée sur la figure), et $F_n$ la force exercée 
%par la bille $n-1$ sur la bille $n$ (comptée également algébriquement).
%
%On rappelle que la force est donnée par la loi de Hertz :
%
%$$ 
%F_n = k \delta_n^{3/2}, \quad k = \frac{4 E \sqrt{a}}{3 (1 - \nu^2)},
%$$
%
%où $\delta_n$ est l'aplatissement des billes $n-1$ et $n$ au niveau de 
%leur contact commun, $E$ le module d'Young du matériau, et $\nu$ le
%coefficient de Poisson.
%
%
%\begin{enumerate}
%
%\item Situation d'équilibre.
%
%On cherche une situation d'équilibre stationnaire telle que 
%$X_n(t) = X_n^0$.
%
% 
%\begin{enumerate}
%
%\item Ecrire la condition d'équilibre de la bille $n$. 
%En déduire que les billes ont toutes le même applatissement,
%noté $\delta_0$.
%
%\item 
%Donnez la valeur de $X_n^0$ en fonction de $n$, $a$ et $F$.
%
%\item Exprimez la variation de longueur $L-L_0$, (où $L_0 = 2na$ est la 
%longueur du système lorque $F=0$) en fonction de $F$ , $L_0$, $k$.
%
%Réponse :
%
%$$
%L-L_0 = - \frac{L_0}{a} \left( \frac{F}{k} \right)^{2/3}
%$$
%
%
%\item On envisage maintenant le cas d'une barre homogène de section
%$S = \pi a^2$ équivalente à celle de la rangée de bille. Donnez
%la relation entre $L-L_0$ et $F$ dans ce second cas. Comparez.
%
%
%
%\item Application numérique :
%Donnez la valeur $L-L_0$ pour une rangée de billes puis pour
%une barre homogène, pour les données suivantes :
%
%$F = ??, E = ??, \nu = ??, a = 1mm, L = 50cm$.
%
%
%
%
%\end{enumerate}
%
%
%
%\item Etude des ondes transversales.
%
%On suppose maintenant que la position instantanée du centre de gravité 
%de la bille $n$ est donnée par $X_n(t) = X_n^0+ U_n(t)$, où
%$X_n^0$ est la position d'équilibre étudiée précédement, et
%$U_n$ le déplacement, supposé faible (cf. figure). 
%
%\begin{enumerate}
%
%\item Exprimez $F_n$ en fonction du déplacement des billes, 
%et faites un développement limité.
%
%Réponse :
%$$
%F_n = F + \frac{3}{2} k \delta_0^{1/2} (U_n - U_{n-1})
%$$
%
%\item En appliquant le principe fondamental de la dynamique, exprimez
%$\ddot U_n$ en fonction de $U_n$, $U_{n+1}$,  $U_{n-1}$.
% 
% Réponse :
%$$
%\ddot U_n = -c^2/a^2 (U_{n+1}-2 U_n + U_{n+1}), c^2 = 
%$$
%
%\item
%On introduit une fonction continue $U(x,t)$ telle que $U(X_n^0,t) = U_n(t)$.
%Par un développement limité, montrez que cette fonction obéit 
%à une équation des ondes dont on exprimera la célérité $c$.
%
%
%\item Application. Calculez $c$ pour les valeurs données précédemment.
%
%\item Comparez avec le cas d'une barre homogène de section $S$ composée 
%du même matériau.
%
%
%
%\end{enumerate}
%
%\item Application : Onde stationnaire dans une dune.
%
%On considère maintenant un empilement vertical de billes.
%
%La situation est la même mais la direction $x$ correspond
%maintenant à la verticale descendante  
%
%La bille 1 est au sommet de la pile, et ne subit pas de force
%de compression ($F_1 = 0$).
%
%
%
%
%
%
%
%
%\begin{enumerate}
%
%\item 
%Dans le cas statique, exprimez la force $F_n$ exercée sur une bille
%par la précédente en fonction de sa profondeur $x \approx 2 n a $ 
%dans la pile.
%
%\item En supposant que le résultat de la question précédente
%reste valable en remplacant la compression $F$ par sa valeur locale
%$F_n$, exprimez la vitesse du son $c$ en fonction de la profondeur.
%
%\item Evaluez, par le calcul d'une intégrale ou par une analyse dimensionnelle,
%la période $T$ mise par une onde pour faire un aller-retour dans
%la pile, en fonction de la profondeur $L$ de celle-ci. 
% 
%
%
%\end{enumerate}


%\end{enumerate}

